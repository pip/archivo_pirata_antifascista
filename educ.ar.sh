#!/bin/bash

if test -z "$1"; then
  echo "Descarga recursos de educ.ar a partir de una página de resultados" >&2
  echo "" >&2
  echo "Modo de uso:" >&2
  echo "  $0 https://www.educ.ar/buscador?persons=3" >&2
  exit 1
fi

url="$1"
path="${PWD}/${1#https://}"
log="${path%/*}.log"
links="${path%/*}.links.log"

function log() {
  logger -t educ.ar -s "$@" 2>&1 | tee -a "${log}"
}

function links() {
  grep -o "https\?://[^\"']\+"
}

function fetch() {
  mkdir -p stats/{dns,server,site,tls}

  wget2 \
    -x \
    --stats-dns="stats/dns/educ.ar.txt" \
    --stats-tls="stats/tls/educ.ar.txt" \
    --stats-server="stats/server/educ.ar.txt" \
    --stats-site="stats/site/educ.ar.txt" \
    --compression="identity,gzip,br" \
    $@
}

pages="`wget2 -qO - "$1" | grep ltima | grep buscador | grep -oE "page=[0-9]+" | cut -d = -f 2`"
pages="${pages:-1}"

if echo "${url}" | grep -q "?" ; then
  separator="&"
else
  separator="?"
fi

log "Descargando páginas: ${pages}"

for page in `seq 1 ${pages}`; do
  echo "${url}${separator}page=${page}"
done | fetch -i -

log "Extrayendo recursos"
log "Recursos encontrados: `cat "${path}${separator}page="* | links | grep /recursos/ | sort -u | tee -a "${links}" | wc -l`"
log "Descargando"

fetch \
  -i "${links}" \
  --page-requisites \
  --convert-links \
  --adjust-extension

log "Terminado"
echo -e "\a"

exit $?
