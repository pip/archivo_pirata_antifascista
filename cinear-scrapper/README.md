# Cinear Scrapper
Descarga series de cinear, tiene un nombre de usuario y contraseña hardcodeados.

## Requerimientos

- NodeJs > 16
- FFmpeg

## Modo de uso

```
$ node cinear.js [id_serie]
```

El id de la serie es el primer parámetro numérico de la URL cuando estás viendo una serie.

## TODO

- Enumerar todas las series
- Verificar funcionamiento con películas
- Mejorar el código
- Hacer tandas de descargas para no saturar la conexión

##

pasos manuales extraidos de https://copiona.com/consola-web-tu-mejor-amiga-II/

1. Hay que estar logueado en el sitio cine.ar
2. Al seleccionar una película y darle play buscar la respuesta de convert.php inspeccionando el sitio
 
3. La respuesta tiene la lista de resoluciones de acuerdo a la velocidad de
tu navegación y te sirve el mas optimo para tu conexión. las playlist estan
en base64, por ejemplo esta en 720p:

```
#EXT-X-STREAM-INF:BANDWIDTH=2667000,NAME="720p (2350k)",RESOLUTION=1330x720,CLOSED-CAPTIONS=NONE
data:application/x-mpegurl;base64,I0VYVE0zVQojRVhULVgtVkVSU0lPTjozCiNFWF(...)
```

4. copiar esa linea a un playlist.txt sin el "data:application/x-mpegurl;base64,"
5. para decodificar en una shell: `$base64 --decode playlist.txt > playlist_d.txt` 
6. con `ffmpeg` podemos descargar la lista y que lo una al finalizar
7. `ffmpeg -protocol_whitelist file,http,https,tcp,tls -i playlist_d.txt -bsf:a aac_adtstoasc -vcodec copy -c copy -crf 0 movie.mp4
8. se descarga la pelicula
