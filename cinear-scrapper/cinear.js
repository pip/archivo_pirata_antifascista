const fs = require("fs");
var childProc = require('child_process');

const email = "vjbcs793t@mozmail.com";
const password = "_6Evr9BJ>-_d6m";

const playlistpath = __dirname + "/playlists";
const downloadpath = __dirname + "/downloads/"


const serieid = process.argv[2];

// console.log(process.argv);

if (!serieid) { 
    console.log("Por favor llamar al script con el id de la serie a descargar, ej: node cinear.js 4175")
    process.exit(-1);
}

let seriestitle = "";
// Pedir token 
fetch("https://id.cine.ar/v1.5/auth/login", {
    "credentials": "omit",
    "headers": {
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0",
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3",
        "Content-Type": "application/json;charset=utf-8",
        "Sec-GPC": "1",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-site",
        "Pragma": "no-cache",
        "Cache-Control": "no-cache"
    },
    "referrer": "https://play.cine.ar/",
    "body": "{\"email\":\""+email+"\",\"password\":\":"+password+"\"}",
    "method": "POST",
    "mode": "cors"
}).then(async res=> {
    let { token } = await res.json();
    console.log("Iniciando sesión con credenciales");


    // //Pedir informacion de usuario
    fetch("https://play.cine.ar/api/v1.7/user", {
        "credentials": "include",
        "headers": {
            "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0",
            "Accept": "application/json, text/plain, */*",
            "Accept-Language": "es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3",
            "Accept-Encoding": "br",
            "Authorization": "Bearer "+ token,
            "Sec-GPC": "1",
            "Sec-Fetch-Dest": "empty",
            "Sec-Fetch-Mode": "cors",
            "Sec-Fetch-Site": "same-origin",
            "Pragma": "no-cache",
            "Cache-Control": "no-cache"
        },
        "referrer": "https://play.cine.ar/INCAA/produccion/4715/reproducir/4716",
        "method": "GET",
        "mode": "cors"
    }).then(async res=> {
        let json = await res.json();
        let perfilid = json.perfiles[0].id;
        console.log("pedir informacion de usuario",perfilid);
        
        
        fetch("https://play.cine.ar/api/v1.7/INCAA/prod/"+serieid+"?perfil="+perfilid, {
            "credentials": "include",
            "headers": {
                "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0",
                "Accept": "application/json, text/plain, */*",
                "Accept-Language": "es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3",
                "Authorization": "Bearer "+ token,
                "Accept-Encoding": "br",

                "Sec-GPC": "1",
                "Sec-Fetch-Dest": "empty",
                "Sec-Fetch-Mode": "cors",
                "Sec-Fetch-Site": "same-origin",
                "Pragma": "no-cache",
                "Cache-Control": "no-cache"
            },
            "referrer": "https://play.cine.ar/INCAA/produccion/4715/reproducir/4716",
            "method": "GET",
            "mode": "cors"
        }).then(async res=> {
            let json = await res.json();
            // console.log(json);
            if (json.items) {
                downloadSerie(perfilid,token,json);
            }
            else {
                downloadPeli(perfilid,token,json);
            }
        })
        
    })
    
})

function downloadPeli(perfilid,token,json) {
    console.log("Esto es una peli, todavía no funciona");
    process.exit();
}

function downloadSerie(perfilid,token,json) {
    json.items.map(item => {

        console.log("pedir informacion del episodio",item.tit,item.sid);

        fetch("https://play.cine.ar/api/v1.7/INCAA/prod/"+item.sid+"?perfil="+perfilid, {
            "credentials": "include",
            "headers": {
                "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0",
                "Accept": "application/json, text/plain, */*",
                "Accept-Language": "es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3",
                "Authorization": "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJkaWQiOiI2NTZiZDQ2MmMwZDg3ZTA0YWM1N2ZlYTMiLCJpYXQiOjE3MDE1NjUzNTgsImp0aSI6IjY1NmJkNDYyYzBkODdlMDRhYzU3ZmVhNCIsInN1YiI6IjY1NmJkMTM1NWU5NzA1MDRiYjlkYzc0ZCIsImV4cCI6MTcxNzExNzUzOH0.WI6dfmacdXU0sqfrmnKUms39FmpwuqfDC5uaX3XGqONHLEPgTxmv8ejeMoRDhAkPyyBSBSf21Qr6CEZ4lfhsowwYFop7VZQq96LJ1U6KheZGJkTX583rqqgqS1c_jVyiIOss3c9OsBDwQZb0xb-IaDHjIoHYDPU6ags-A0YajALT2b63oYMwECn7Y4qomXfdRrgLyn4IOOVfjOk_Pt38j8zi7YLV_5jshvWU9aqcN2-jFe_sDaDdh1fCyKVpS4noJH7eVMjkrRcc7e2qo8pvG-mQyYHzLHjFMUz3uOc0uBuxkC7SDHdWDbR5IC7A2rhw_aD7EvrlRS8_ksp-F4zz1Q",
                "Sec-GPC": "1",
                "Accept-Encoding": "br",

                "Sec-Fetch-Dest": "empty",
                "Sec-Fetch-Mode": "cors",
                "Sec-Fetch-Site": "same-origin",
                "Pragma": "no-cache",
                "Cache-Control": "no-cache"
            },
            "referrer": "https://play.cine.ar/INCAA/produccion/4715",
            "method": "GET",
            "mode": "cors"
        }).then(async res=> {
            let json = await res.json();
            // console.log("informacion del episodio",item.sid);


            fetch("https://player.cine.ar/odeon/?i="+item.sid+"&p="+perfilid+"&s=INCAA&t="+token, {
                "credentials": "omit",
                "headers": {
                    "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0",
                    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
                    "Accept-Language": "es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3",
                    "Sec-GPC": "1",
                    "Upgrade-Insecure-Requests": "1",
                    "Sec-Fetch-Dest": "iframe",
                    "Sec-Fetch-Mode": "navigate",
                    "Sec-Fetch-Site": "same-site",
                    "Pragma": "no-cache",
                    "Cache-Control": "no-cache"
                },
                "referrer": "https://play.cine.ar/",
                "method": "GET",
                "mode": "cors"
            }).then(async res=> {
                let text = await res.text();
                let objecttext = text.split("optionsContent.playlist = ")[1].split(";")[0];
                let object = eval(objecttext);
                let episodeinfo = object[0];
                if (!seriestitle) {
                    seriestitle = episodeinfo.title;
                }
                // console.log(episodeinfo);
                let downloadfilename = seriestitle+"/"+(episodeinfo.description || episodeinfo.title || item.sid)+".mp4"

                episodeinfo.sources.map(source => {
                    // console.log(source.file);

                    fetch("https://player.cine.ar/odeon/"+source.file, {
                        "credentials": "omit",
                        "headers": {
                            "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0",
                            "Accept": "*/*",
                            "Accept-Language": "es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3",
                            "Sec-GPC": "1",
                            "Sec-Fetch-Dest": "empty",
                            "Sec-Fetch-Mode": "cors",
                            "Sec-Fetch-Site": "same-origin",
                            "Pragma": "no-cache",
                            "Cache-Control": "no-cache"
                        },
                        "referrer": "https://player.cine.ar/odeon/?i=4716&p=656bd197189ad425aec69870&s=INCAA&t=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJkaWQiOiI2NTZiZDQ2MmMwZDg3ZTA0YWM1N2ZlYTMiLCJpYXQiOjE3MDE1NjUzNTgsImp0aSI6IjY1NmJkNDYyYzBkODdlMDRhYzU3ZmVhNCIsInN1YiI6IjY1NmJkMTM1NWU5NzA1MDRiYjlkYzc0ZCIsImV4cCI6MTcxNzExNzUzOH0.WI6dfmacdXU0sqfrmnKUms39FmpwuqfDC5uaX3XGqONHLEPgTxmv8ejeMoRDhAkPyyBSBSf21Qr6CEZ4lfhsowwYFop7VZQq96LJ1U6KheZGJkTX583rqqgqS1c_jVyiIOss3c9OsBDwQZb0xb-IaDHjIoHYDPU6ags-A0YajALT2b63oYMwECn7Y4qomXfdRrgLyn4IOOVfjOk_Pt38j8zi7YLV_5jshvWU9aqcN2-jFe_sDaDdh1fCyKVpS4noJH7eVMjkrRcc7e2qo8pvG-mQyYHzLHjFMUz3uOc0uBuxkC7SDHdWDbR5IC7A2rhw_aD7EvrlRS8_ksp-F4zz1Q",
                        "method": "GET",
                        "mode": "cors"
                    }).then(async res=> {
                        let text = await res.text();
                        let resoluciones = text.split("#EXT");
                        let max = resoluciones[resoluciones.length -1].split(",");
                        let playlistenc = max[max.length-1];

                        let buff = Buffer.from(playlistenc, 'base64');
                        let playlist = buff.toString('utf-8');

                        
                        fs.mkdirSync(playlistpath,{recursive: true})
                        // console.log("creating",downloadpath+"/"+seriestitle);
                        fs.mkdirSync(downloadpath+"/"+seriestitle,{recursive: true})
                        
                        const playlistfilename = playlistpath + "/playlist-" + item.sid+".m3u8";
                        const downloadfullpath = downloadpath + downloadfilename;
                        console.log("descargando",item.sid,item.tit," en ",downloadfullpath);
                        const fd = fs.openSync(playlistfilename, 'a');
                        fs.writeFileSync(fd,playlist);
                                                    
                        const childCommand = 'ffmpeg -protocol_whitelist file,http,https,tcp,tls -i '+playlistfilename+' -bsf:a aac_adtstoasc -vcodec copy -c copy -crf 0 "'+downloadfullpath+'"';
                
                        childBrowser = childProc.exec(childCommand, (error) => {
                            console.log("FFmpeg process ended",childCommand,error);
                        });
                
                    })
                })
                  
            })             
        });                
    })

}