#!/bin/sh
#
# Fallar al primer error
set -e

if test -z "$1"; then
  echo "Modo de uso:" >&2
  echo "  $0 https://argentina.gob.ar" >&2
  exit 1
fi

cur="$PWD"
# El archivo de registro
log="`echo "$1" | cut -d / -f 3 | cut -d : -f 1`.`date +%F`.log"
url="$1"

# ¿Soportamos wget2?
WGET2="`command wget2 >/dev/null 2>&1 ; echo $?`"

if test ${WGET2} -eq 0; then
  wget=wget2
  extra_flags="
    --stats-dns=${cur}/stats/dns/${log%log}csv
    --stats-tls=${cur}/stats/tls/${log%log}csv
    --stats-server=${cur}/stats/server/${log%log}csv
    --stats-site=${cur}/stats/site/${log%log}csv
    --compression=identity,gzip,br
  "
else
  wget=wget
  extra_flags="
  --rejected-log=${cur}/stats/site/${log%log}csv
  "
fi

# Recolectar estadísticas
mkdir -p ${cur}/stats/{dns,server,site,tls}

echo "Archivando ${url}"
echo "El registro será guardado en ${cur}/${log}"

test ${WGET2} -eq 0 && echo "Las estadísticas serán guardadas en ${cur}/stats/"

echo "Podés seguir la descarga desde otra terminal con:"
echo "tail -f ${cur}/${log}"

# Empezar la descarga
$wget \
  --random-wait \
  --retry-on-http-error=503,504,429 \
  --user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/119.0" \
  --mirror \
  --page-requisites \
  --adjust-extension \
  --trust-server-names \
  --append-output="${cur}/${log}" \
  ${extra_flags} \
  $@
ret=$?

echo -e "Descarga terminada\a"
date
exit $ret
