# Archivo Pirata Antifascista

## Cómo instalar

**Importante:** Por el momento este programa sólo funciona en Linux.

Por velocidad de descarga y urgencia es importante contar con el
programa [wget2](https://gitlab.com/gnuwget/wget2), aunque no está
disponible en todos los repositorios de software.

### Posibles formas de instalación de wget2 y otras dependencias

En Debian, Ubuntu, Mint:

```sh
sudo apt install wget2 git
```

En Archlinux, Parabola, Manjaro:

```sh
sudo yay -S wget2 git
```

**Nota:** Si wget2 no está disponible, todavía necesitamos git.

### Instalar el Archivo Pirata Antifascista

Es necesario contar con `git` instalado (ver punto anterior)

```sh
# Clonar el repositorio nos permite poder actualizarlo luego
git clone https://0xacab.org/pip/archivo_pirata_antifascista
```

## Cómo usar

Primero hay que elegir un sitio y luego archivarlo así:

```sh
# Por cada terminal nueva que abramos, hay que moverse al archivo
cd archivo_pirata_antifascista
```

Luego, por cada sitio:

```sh
./archivo_pirata_antifascista.sh https://argentina.gob.ar
```

El programa da algunas indicaciones y solo es necesario esperar a que
termine de trabajar.

## educ.ar

El programa `educ.ar` permite archivar todos los recursos de una
categoría específica del sitio [educ.ar](https://educ.ar).

Por ejemplo:

```sh
# Un filtro específico
./educ.ar.sh https://www.educ.ar/buscador?persons=3
# Todos los recursos
./educ.ar.sh https://www.educ.ar/buscador
```
